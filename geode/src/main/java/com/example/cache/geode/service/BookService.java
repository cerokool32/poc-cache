package com.example.cache.geode.service;

import com.example.cache.geode.model.Book;
import com.example.cache.geode.repository.BookRepository;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
public class BookService {

    @Autowired
    private BookRepository bookRepository;

    @Cacheable(value = "books", key = "{#id}")
    public Optional<Book> findById(Long id) {
        return bookRepository.findById(id);
    }

    @CacheEvict(value = "books", key = "{#id}")
    public void delete(Long id) {
        bookRepository.deleteById(id);
    }

    @CachePut(value = "books", key = "{#id}")
    public Book update(Long id, String name) throws Exception {
        Optional<Book> book = bookRepository.findById(id);
        if (book.isPresent()) {
            book.get().setName(name);
            return bookRepository.save(book.get());
        } else {
            throw new Exception("book not found");
        }
    }

}
