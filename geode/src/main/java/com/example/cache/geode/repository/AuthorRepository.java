package com.example.cache.geode.repository;

import com.example.cache.geode.model.Author;
import org.springframework.data.repository.CrudRepository;

public interface AuthorRepository extends CrudRepository<Author, Long> {

}
