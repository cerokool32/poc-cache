package com.example.cache.geode.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.gemfire.mapping.annotation.Indexed;
import org.springframework.data.gemfire.mapping.annotation.Region;

@Data
@Region("author")
public class Author {

    @Id
    private Long id;

    @Indexed
    private String name;

}
