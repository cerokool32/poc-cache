package com.example.cache.geode;

import com.example.cache.geode.model.Author;
import com.example.cache.geode.model.Book;
import com.example.cache.geode.model.Publication;
import com.example.cache.geode.repository.BookRepository;
import com.google.common.base.Stopwatch;
import java.time.LocalDate;
import java.util.concurrent.TimeUnit;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.gemfire.cache.config.EnableGemfireCaching;
import org.springframework.data.gemfire.config.annotation.EnableClusterConfiguration;
import org.springframework.data.gemfire.config.annotation.EnableEntityDefinedRegions;
import org.springframework.data.gemfire.config.annotation.EnableEviction;
import org.springframework.data.gemfire.config.annotation.EnableIndexing;
import org.springframework.data.gemfire.config.annotation.EnableLocator;
import org.springframework.data.gemfire.config.annotation.EnableManager;
import org.springframework.data.gemfire.config.annotation.EnablePdx;
import org.springframework.data.gemfire.config.annotation.EnableStatistics;
import org.springframework.data.gemfire.config.annotation.PeerCacheApplication;
import org.springframework.data.gemfire.repository.config.EnableGemfireRepositories;
import uk.co.jemos.podam.api.PodamFactory;
import uk.co.jemos.podam.api.PodamFactoryImpl;
import uk.co.jemos.podam.api.RandomDataProviderStrategyImpl;

@SpringBootApplication
@EnableManager(start = true)
@PeerCacheApplication(name = "GeodeApplication", useClusterConfiguration = true)
@EnableLocator
@EnableClusterConfiguration(useHttp = true)
@EnableEntityDefinedRegions(basePackageClasses = {Book.class})
@EnableGemfireRepositories(basePackageClasses = {BookRepository.class})
@EnableEviction
@EnableIndexing
@EnablePdx
@EnableGemfireCaching
@EnableStatistics
@Slf4j
public class GeodeApplication {

    public static void main(String[] args) {
        SpringApplication.run(GeodeApplication.class, args);
    }

    @Bean
    ApplicationRunner runner(BookRepository bookRepository) {

        return args -> {

            RandomDataProviderStrategyImpl strategy = new RandomDataProviderStrategyImpl();
            PodamFactory podamFactory = new PodamFactoryImpl(strategy);

            Stopwatch stopwatch = Stopwatch.createStarted();
            for (int i = 0; i < 100000; i++) {
                Book book = podamFactory.manufacturePojo(Book.class);
                bookRepository.save(book);
            }
            stopwatch.stop();
            log.info("elapsed time: {}", stopwatch.elapsed(TimeUnit.MILLISECONDS));

            Author author = new Author();
            author.setId(1l);
            author.setName("author 1");

            Publication publication = new Publication();
            publication.setId(1l);
            publication.setReleaseDate(LocalDate.now());

            Book book = new Book();
            book.setId(1l);
            book.setName("book");
            book.setAuthor(author);
            book.setPublishDate(publication);

            bookRepository.save(book);
        };
    }

}


