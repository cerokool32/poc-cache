package com.example.cache.geode.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.gemfire.mapping.annotation.Indexed;
import org.springframework.data.gemfire.mapping.annotation.Region;

@Data
@Region("books")
public class Book {

    @Id
    private Long id;

    @Indexed
    private String name;

    private Publication publishDate;

    private Author author;

}
