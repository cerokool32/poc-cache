package com.example.cache.geode.repository;

import com.example.cache.geode.model.Book;
import org.springframework.data.repository.CrudRepository;

public interface BookRepository extends CrudRepository<Book, Long> {

}
