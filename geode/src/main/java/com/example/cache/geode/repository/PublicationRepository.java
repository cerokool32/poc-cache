package com.example.cache.geode.repository;

import com.example.cache.geode.model.Publication;
import org.springframework.data.repository.CrudRepository;

public interface PublicationRepository extends CrudRepository<Publication, Long> {

}
