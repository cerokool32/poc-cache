package com.example.cache.geode.model;

import java.time.LocalDate;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.gemfire.mapping.annotation.Indexed;
import org.springframework.data.gemfire.mapping.annotation.Region;

@Data
@Region("publication")
public class Publication {

    @Id
    private Long id;

    @Indexed
    private LocalDate releaseDate;

}
