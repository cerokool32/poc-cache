package com.example.cache.geode.service.dto;

import lombok.Data;

@Data
public class UpdateBookName {

    private String name;

}
