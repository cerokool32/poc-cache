package com.example.cache.simple.service.dto;

import lombok.Data;

@Data
public class UpdateBookName {

    private String name;
}
