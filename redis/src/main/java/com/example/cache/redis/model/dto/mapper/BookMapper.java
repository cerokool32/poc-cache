package com.example.cache.redis.model.dto.mapper;

import com.example.cache.redis.model.Book;
import com.example.cache.redis.model.dto.BookDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel="spring", uses = {AuthorMapper.class, PublicationMapper.class})
public interface BookMapper {

    BookDto entityToDto(Book book);


}
