package com.example.cache.redis.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.sql.rowset.serial.SerialArray;
import lombok.Data;
import lombok.ToString;

@Data
@ToString(exclude = {"books"})
@Entity
@Table(name = "publication")
public class Publication implements Serializable {

    public static final long serialVersionUID = 44L;

    @Id
    @Column(name = "publication_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "release_date")
    private LocalDate releaseDate;

    @ManyToMany(mappedBy = "publishDate", fetch = FetchType.LAZY)
    private List<Book> books;
}
