package com.example.cache.redis.repository;

import com.example.cache.redis.model.Publication;
import org.springframework.data.repository.CrudRepository;

public interface PublicationRepository extends CrudRepository<Publication, Integer> {

}
