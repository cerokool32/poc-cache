package com.example.cache.redis.service.dto;

import lombok.Data;

@Data
public class UpdateBookName {

    private String name;
}
