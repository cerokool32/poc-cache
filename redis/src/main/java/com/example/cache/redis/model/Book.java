package com.example.cache.redis.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;
import lombok.ToString;

@Data
@Entity
@Table(name = "book")
public class Book implements Serializable {

    public static final long serialVersionUID = 42L;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "editorial")
    private String editorial;

    @ManyToOne
    @JoinColumn(name = "publication_id")
    private Publication publishDate;

    @ManyToOne
    @JoinColumn(name = "author_id")
    private Author author;
}
