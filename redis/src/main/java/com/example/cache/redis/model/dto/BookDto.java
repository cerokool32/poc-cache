package com.example.cache.redis.model.dto;

import java.io.Serializable;
import lombok.Data;

@Data
public class BookDto {

    private Long id;

    private String name;

    private String editorial;

    private PublicationDto publishDate;

    private AuthorDto author;
}
