package com.example.cache.redis.model.dto;

import java.io.Serializable;
import java.time.LocalDate;
import lombok.Data;

@Data
public class PublicationDto {

    private Long id;

    private String releaseDate;
}
