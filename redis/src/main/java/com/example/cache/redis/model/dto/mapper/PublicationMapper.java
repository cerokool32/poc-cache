package com.example.cache.redis.model.dto.mapper;

import com.example.cache.redis.model.Publication;
import com.example.cache.redis.model.dto.PublicationDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel="spring")
public interface PublicationMapper {

    @Mapping(target = "releaseDate", dateFormat = "dd/MM/yy")
    PublicationDto entityToDto(Publication publication);
}
