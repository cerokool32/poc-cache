package com.example.cache.redis.model.dto;

import java.io.Serializable;
import lombok.Data;

@Data
public class AuthorDto {

    private Long id;

    private String name;
}
