package com.example.cache.redis.service;

import com.example.cache.redis.model.Book;
import com.example.cache.redis.model.dto.BookDto;
import com.example.cache.redis.model.dto.mapper.BookMapper;
import com.example.cache.redis.repository.BookRepository;
import java.util.Optional;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
public class BookService {

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private BookMapper bookMapper;

    @Cacheable(value = "books", key = "{#id}")
    public Optional<BookDto> findById(Integer id) {
        return Optional.ofNullable(bookMapper.entityToDto(bookRepository.findById(id).get()));
    }

    @CacheEvict(value = "books", key = "{#id}")
    public void delete(Integer id) {
        bookRepository.deleteById(id);
    }

    @CachePut(value = "books", key = "{#id}")
    public BookDto update(Integer id, String name) throws Exception{
        Optional<Book> book = bookRepository.findById(id);
        if(book.isPresent()) {
            book.get().setName(name);
            return bookMapper.entityToDto(bookRepository.save(book.get()));
        } else {
            throw new Exception("book not found");
        }
    }
}
