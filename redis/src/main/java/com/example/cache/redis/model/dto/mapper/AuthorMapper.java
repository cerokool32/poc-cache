package com.example.cache.redis.model.dto.mapper;

import com.example.cache.redis.model.Author;
import com.example.cache.redis.model.dto.AuthorDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel="spring")
public interface AuthorMapper {

    AuthorDto entityToDto(Author author);
}
