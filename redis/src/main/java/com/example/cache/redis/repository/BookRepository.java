package com.example.cache.redis.repository;

import com.example.cache.redis.model.Book;
import org.springframework.data.repository.CrudRepository;

public interface BookRepository extends CrudRepository<Book, Integer> {

}
