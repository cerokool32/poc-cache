package com.example.cache.redis.controller;

import com.example.cache.redis.model.Book;
import com.example.cache.redis.model.dto.BookDto;
import com.example.cache.redis.service.BookService;
import com.example.cache.redis.service.dto.UpdateBookName;
import com.google.common.base.Stopwatch;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import javax.xml.ws.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/cache")
public class BookController {

    @Autowired
    private BookService bookService;

    @GetMapping("/{id}")
    public ResponseEntity<String> retrieveBook(
            @PathVariable("id")
                    Integer id) {
        Stopwatch stopwatch = Stopwatch.createStarted();
        Optional<BookDto> book = bookService.findById(id);
        stopwatch.stop();
        log.info("Query time: {}", stopwatch.elapsed(TimeUnit.MILLISECONDS));
        if (book.isPresent()) {
            return ResponseEntity.ok(book.get().toString());
        }
        return ResponseEntity.notFound().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(
            @PathVariable("id")
                    Integer id) {
        Stopwatch stopwatch = Stopwatch.createStarted();
        bookService.delete(id);
        stopwatch.stop();
        log.info("Delete time: {}", stopwatch.elapsed(TimeUnit.MILLISECONDS));
        return ResponseEntity.noContent().build();

    }

    @PostMapping("/{id}")
    public ResponseEntity<String> update(
            @PathVariable("id")
                    Integer id,
            @RequestBody
                    UpdateBookName updateBookName) {
        Stopwatch stopwatch = Stopwatch.createStarted();
        try {
            BookDto book = bookService.update(id, updateBookName.getName());
            stopwatch.stop();
            log.info("Update time: {}", stopwatch.elapsed(TimeUnit.MILLISECONDS));
            return ResponseEntity.ok(book.toString());
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }

}
