package com.example.cache.redis.repository;

import com.example.cache.redis.model.Author;
import org.springframework.data.repository.CrudRepository;

public interface AuthorRepository extends CrudRepository<Author, Integer> {

}
