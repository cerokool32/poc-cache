package com.example.cache.elasticcache.config;

import com.amazonaws.regions.RegionUtils;
import com.amazonaws.services.elasticache.AmazonElastiCacheClient;
import com.google.common.collect.Lists;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.Cache;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.aws.cache.ElastiCacheFactoryBean;
import org.springframework.cloud.aws.cache.memcached.MemcachedCacheFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

@Configuration
@EnableCaching
@Slf4j
public class RedisConfig {

    private static final int EMBEDDED_PORT = 11211;

    @Value("${aws.elasticache.cluster.id}")
    String cacheClusterId;

    @Value("${aws.region}")
    String regionName;

    @Value("${aws.elasticache.expiry.seconds}")
    int cacheExpirySeconds;

    @Bean(name = "beamCache")
    @Lazy
    Cache getCache() throws Exception {
        final MemcachedCacheFactory memcachedCacheFactory = new MemcachedCacheFactory();
        memcachedCacheFactory.setExpiryTime(cacheExpirySeconds);

        // We are only interested in memcached
        List cacheFactories = Lists.newArrayList();
        cacheFactories.add(memcachedCacheFactory);
        // Setup the aws elasticache client
        AmazonElastiCacheClient amazonElastiCacheClient = new AmazonElastiCacheClient();
        amazonElastiCacheClient.setRegion(RegionUtils.getRegion(regionName));
        // Use the factory to produce the cache
        ElastiCacheFactoryBean elastiCacheFactoryBean = new ElastiCacheFactoryBean(amazonElastiCacheClient,
                cacheClusterId, cacheFactories);
        elastiCacheFactoryBean.afterPropertiesSet();
        return elastiCacheFactoryBean.getObject();

    }
}
