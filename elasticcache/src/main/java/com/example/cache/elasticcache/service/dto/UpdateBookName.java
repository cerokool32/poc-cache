package com.example.cache.elasticcache.service.dto;

import lombok.Data;

@Data
public class UpdateBookName {

    private String name;
}
