package com.example.cache.elasticcache.repository;

import com.example.cache.elasticcache.model.Publication;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PublicationRepository extends CrudRepository<Publication, Long> {

}
